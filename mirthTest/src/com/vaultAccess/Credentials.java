package com.vaultAccess;

import org.junit.Before;
import se.jhaals.*;

import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class Credentials {

        private String token = "s.n915ITlrdKMbGipY5Tbg9kjK"; //System.getenv("VAULT_TOKEN");
        private String vault_server_url = "http://localhost:8200";
        private Vault vault;

        @Before
        public void setUp() {
            this.vault = new se.jhaals.Vault(vault_server_url, token);

        }

        public String testWrite() throws Exception {

            Map<String, String> data = new HashMap<>();
            data.put("user", "password2");
            Response response = vault.write("credentials/user", data);
            return response.toString();
        }

        public void testRead(String path, String user) throws Exception {
            VaultResponse result = vault.read(path);

            System.out.println("TEST-Read:" + result.toString());
        }
}
